package tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import model.User;

class TestUser {

	@Test
	void test1() {
		User u = new User();
		assertEquals(true, u.inferieur10(1));
	}
	
	@Test
	void test11() {
		User u = new User();
		assertEquals(false, u.inferieur10(11));
	}

}
