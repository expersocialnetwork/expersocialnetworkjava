package model;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

@Entity
public class Publication implements Serializable{
	

	private Long id;
	
	private String content;
	private Date createdAt;
	private Date updatedAt;
	

	private User createdBy;	
	private Set<User> likedBy = new HashSet<User>();

	public Publication() {
		super();
	}

	
	@Id
	@Column(name = "Publication_Id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "User_Id", nullable = false)
	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}


	@ManyToMany(fetch = FetchType.EAGER)
	public Set<User> getLikedBy() {
		return likedBy;
	}


	public void setLikedBy(Set<User> likedBy) {
		this.likedBy = likedBy;
	}
	
	public void addUserWhoLiked(User userWhoLiked) {
		this.likedBy.add(userWhoLiked);
	}
	
	public void deleteUserWhodisliked(User userWhodisliked) {
		this.likedBy.remove(userWhodisliked);
	}
	
	 @PrePersist
	  void createdAt() {
	    this.createdAt = this.updatedAt = new Date();
	  }
	 
	 @PreUpdate
	  void updatedAt() {
	    this.updatedAt = new Date();
	  }
	

	

}
