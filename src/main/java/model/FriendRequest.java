package model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
public class FriendRequest implements Serializable {
	
	
	private Long id;		
	private User requestingUser;
	private User requestedUser;
	
	public FriendRequest() {
		super();
		/**/
	}
	
	

	public FriendRequest(User requestingUser, User requestedUser) {
		super();
		this.requestingUser = requestingUser;
		this.requestedUser = requestedUser;
	}



	@Id
	@Column(name = "Friend_Request_Id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}



	public User getRequestingUser() {
		return requestingUser;
	}



	public void setRequestingUser(User requestinUser) {
		this.requestingUser = requestinUser;
	}



	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "User_Id", nullable = false)
	public User getRequestedUser() {
		return requestedUser;
	}

	public void setRequestedUser(User requestedUser) {
		this.requestedUser = requestedUser;
	}
	
	
	
	


}
