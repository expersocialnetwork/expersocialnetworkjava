package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
@Table( name="experuser")
public class User implements Serializable {
	
	

	private Long id;	
	private String firstname;
	private String lastname;
	private String username;
	private String email;
	private String password;
	private Set<Publication> createdPublications = new HashSet<Publication>(0);		
	private Set<Publication> likedPublications = new HashSet<Publication>();
	private Set<User> friends = new HashSet<User>();
	private Set<FriendRequest> friendRequests = new HashSet<FriendRequest>();

	public User() {
		super();
		
	}
	
	
	@Id
	@Column(name = "User_Id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	
	@OneToMany(fetch = FetchType.EAGER, mappedBy="createdBy")
	public Set<Publication> getCreatedPublications() {
		return createdPublications;
	}

	public void setCreatedPublications(Set<Publication> createdPublications) {
		this.createdPublications = createdPublications;
	}


	@ManyToMany(fetch = FetchType.EAGER,mappedBy="likedBy")
	public Set<Publication> getLikedPublications() {
		return likedPublications;
	}


	public void setLikedPublications(Set<Publication> likedPublications) {
		this.likedPublications = likedPublications;
	}

	@ManyToMany(fetch = FetchType.EAGER)
	public Set<User> getFriends() {
		return friends;
	}

	public void setFriends(Set<User> friends) {
		this.friends = friends;
	}


	public void addFriend(User friend) {
		this.getFriends().add(friend);
		/**//**/
		
	}

	@OneToMany(fetch = FetchType.EAGER, mappedBy="requestedUser")
	public Set<FriendRequest> getFriendRequests() {
		return friendRequests;
	}

	
	public void setFriendRequests(Set<FriendRequest> friendRequests) {
		this.friendRequests = friendRequests;
	}
	
	public void addFriendRequest(FriendRequest friendRequest) {
		this.getFriendRequests().add(friendRequest);
		/**//**/
		
	}

	public void removeFriendRequest(FriendRequest friendRequest) {
		this.getFriendRequests().remove(friendRequest);	
		/**//**/
		
	}
	
	
	public Boolean inferieur10(int unChiffre) {
		boolean varRetour = false;
		if (unChiffre < 10) {
			varRetour = true;	
		}else {
			varRetour = false;
		}
		return varRetour;
	}

	
	
	

}
