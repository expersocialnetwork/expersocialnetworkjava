package rest;

import java.util.List;
import java.util.Set;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.Response.Status;

import model.FriendRequest;
import model.Publication;
import model.User;

@Stateless
@Path("/posts")
public class PublicationEndpoint {
	@PersistenceContext(unitName = "primary")
	private EntityManager em;
	
	

	
	@GET
	@Produces("application/json")
	public List<Publication> listFriendsublication(@QueryParam("id") Integer userId,
			@QueryParam("max") Integer maxResult) {
		TypedQuery<Publication> findAllQuery = em.createQuery(
				"SELECT DISTINCT p FROM Publication p ORDER BY p.createdAt DESC", Publication.class);

		final List<Publication> results = findAllQuery.getResultList();
		return results;
	}
	//WHERE p.createdBy.id IN (SELECT DISTINCT u.friends FROM User u WHERE u.id = userId) ORDER BY p.id
	
	@POST
	@Consumes("application/json")
	public Response create(Publication  entity) {
		em.persist(entity);
        // em.getTransaction().commit();
		Publication publication = em.find(Publication.class, entity.getId());
		return Response.ok(publication).build();
	}
	
	
	@Path("likerequest/{userId}/{publicationId}")
	@POST
	@Consumes("application/json")
	public Response like(@PathParam("userId") Long userId,@PathParam("publicationId") Long publicationId) {	
		User userWhoLiked = em.find(User.class, userId);
		Publication publicationLiked = em.find(Publication.class, publicationId);
		publicationLiked.addUserWhoLiked(userWhoLiked);
		em.merge(publicationLiked);
		return Response.ok().build();
		
		
	}
	
	@Path("dislikerequest/{userId}/{publicationId}")
	@POST
	@Consumes("application/json")
	public Response dislike(@PathParam("userId") Long userId,@PathParam("publicationId") Long publicationId) {	
		User userWhoDisliked = em.find(User.class, userId);
		Publication publicationLiked = em.find(Publication.class, publicationId);
		publicationLiked.deleteUserWhodisliked(userWhoDisliked);
		em.merge(publicationLiked);

		return Response.ok().build();
		
		
	}
	
	@Path("isPostLiked/{userId}/{publicationId}")
	@POST
	@Consumes("application/json")
	public Response isPostLiked(@PathParam("userId") Long userId,@PathParam("publicationId") Long publicationId) {	
		User user = em.find(User.class, userId);
		Publication publication = em.find(Publication.class, publicationId);
		System.out.println(publication.getLikedBy().contains(user));
		return Response.ok(publication.getLikedBy().contains(user)).build();
	}
	
	
	
	
	


}
