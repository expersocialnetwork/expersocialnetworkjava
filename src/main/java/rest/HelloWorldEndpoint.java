package rest;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.TypedQuery;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;



@Stateless
@Path("/helloworld")
public class HelloWorldEndpoint {
	
	
	
	@GET
	@Produces("application/json")
	public String HelloWorld() {
		return "Hello EXPERNET from Java !";
	}	

}
