package rest;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.Response.Status;


import model.FriendRequest;
import model.SearchUserResult;
import model.User;

@Stateless
@Path("/users")
public class UserEndpoint {
	@PersistenceContext(unitName = "primary")
	private EntityManager em;	
	
	@GET
	@Produces("application/json")
	public List<User> listAll(@QueryParam("start") Integer startPosition,
			@QueryParam("max") Integer maxResult) {
		TypedQuery<User> findAllQuery = em.createQuery(
				"SELECT DISTINCT u FROM User u  ORDER BY u.id", User.class);
		if (startPosition != null) {
			findAllQuery.setFirstResult(startPosition);
		}
		if (maxResult != null) {
			findAllQuery.setMaxResults(maxResult);
		}
		final List<User> results = findAllQuery.getResultList();
		return results;
	}
	
	@GET
	@Path("{id}")
	@Produces("application/json")
	public User userInfo(@PathParam("id") Long id)
			 {
		User userfound =  em.find(User.class, id);	
		
		return userfound;
	}
	
	@GET
	@Path("search/{term}")
	@Produces("application/json")
	public List<SearchUserResult> search(@PathParam("term") String term)			 {		
		TypedQuery<SearchUserResult> findAllQuery = (TypedQuery<SearchUserResult>) em.createNativeQuery(
				"SELECT DISTINCT experuser.User_Id as id, experuser.firstname, experuser.lastname, experuser.username, experuser.email FROM experuser  WHERE LOWER(firstname) LIKE '%' || :param || '%' OR LOWER(lastname) LIKE '%' || :param || '%' OR LOWER(username) LIKE '%' || :param || '%' ", SearchUserResult.class);	
		findAllQuery.setParameter("param", term.toLowerCase());
		final List<SearchUserResult> results = findAllQuery.getResultList();
		return results;
	}
	
	@POST
	@Path("register")
	@Consumes("application/json")
	public Response create(User entity) {
		em.persist(entity);
		return Response.created(
				UriBuilder.fromResource(UserEndpoint.class)
						.path(String.valueOf(entity.getId())).build()).build();
	}
	
	@Path("friendslist/{id}")
	@GET
	@Produces("application/json")
	public List<SearchUserResult> getFriendlist(@PathParam("id") Long myId) {		
		User me =  em.find(User.class, myId);			
		TypedQuery<SearchUserResult> findAllQuery = (TypedQuery<SearchUserResult>) em.createNativeQuery(
				"SELECT DISTINCT experuser.User_Id as id, experuser.firstname, experuser.lastname, experuser.username, experuser.email FROM experuser WHERE experuser.user_id IN (SELECT tj.user_user_id FROM experuser_experuser tj WHERE tj.friends_user_id = :paramId)\r\n" + 
				"OR experuser.user_id IN (SELECT tj.friends_user_id FROM experuser_experuser tj WHERE tj.user_user_id = :paramId)", SearchUserResult.class);	
		findAllQuery.setParameter("paramId", myId);
		final List<SearchUserResult> results = findAllQuery.getResultList();
		return results;
	}

	
	@Path("friendrequest/{myId}/{friendId}")
	@POST
	@Consumes("application/json")
	public Response sendFriendRequest(@PathParam("myId") Long myId,@PathParam("friendId") Long friendId) {	
		User me =  em.find(User.class, myId);
		User friend =  em.find(User.class, friendId);		
		FriendRequest friendrequest = new FriendRequest(me, friend);		
		em.persist(friendrequest);
		if(me ==null | friend == null) {			
			return Response.status(Status.NOT_FOUND).build();		
		}		
		//friend.addFriendRequest(new FriendRequest(me.getId()));		
		return Response.ok().build();
	}
	
	
	@Path("acceptfriendrequest/{id}")
	@POST
	@Consumes("application/json")
	public Response addFriend(@PathParam("id") Long friendrequestId) {	
		
		FriendRequest friendRequest = em.find(FriendRequest.class, friendrequestId);
		User me = em.find(User.class,friendRequest.getRequestedUser().getId());
		User requestingUser =  em.find(User.class,friendRequest.getRequestingUser().getId());
				//friendRequest.getRequestingUser();
		//em.find(User.class,friendRequest.getRequestingUserId());	
		if(me ==null | requestingUser == null) {			
			return Response.status(Status.NOT_FOUND).build();		
		}	
		requestingUser.addFriend(me);
		me.removeFriendRequest(friendRequest);
		em.remove(friendRequest);
		
		return Response.ok(requestingUser.getId()).build();
	}
	
	@Path("test")
	@POST
	@Consumes("application/json")
	public Response test() {		
		return Response.ok("test auth est passe !").build();
	}
	
	
	
	

	

	
	@Path("friends")
	@GET
	@Produces("application/json")
	public  List<User> listFriends(@QueryParam("id") Integer userId) {

		TypedQuery<User> findAllQuery = em.createQuery(
				"SELECT DISTINCT u.friends FROM User u WHERE u.id = userId", User.class);
		final List<User> results = findAllQuery.getResultList();
		return results;
	}

	

}
