package rest;


import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Path;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.xml.bind.DatatypeConverter;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


@WebFilter(urlPatterns = {"/api/*"}, description = "JWT Auth Filter")
public class AuthFilter implements Filter {
	
	   private static final Set<String> ALLOWED_PATHS = Collections.unmodifiableSet(new HashSet<>(
		        Arrays.asList( "/api/login", "/api/users/register")));

	@Override
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
			throws IOException, ServletException {	
		HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;
        
        String path = request.getRequestURI().substring(request.getContextPath().length()).replaceAll("[/]+$", ""); 
        boolean allowedPath = ALLOWED_PATHS.contains(path);

        if (allowedPath) {
            chain.doFilter(req, res);
            return;
        }
       
        String authorization = request.getHeader("Authorization");
        System.out.println(authorization);
        if (authorization != null ) 
        {
        	try {
        	String jwt = authorization.substring("bearer".length()).trim(); 
            Claims claims = Jwts.parser()         
            	       .setSigningKey(DatatypeConverter.parseBase64Binary("javaServerSecret"))
            	       .parseClaimsJws(jwt).getBody();        
            	    if(claims != null) {            	    	
            	    	chain.doFilter(req, res);            	    	
            	    }else {
                    	response.sendError(HttpServletResponse.SC_UNAUTHORIZED);                    	
                    } 
        	}catch(Exception e) {
        		response.sendError(HttpServletResponse.SC_UNAUTHORIZED);        		
        	}
        }else {
        	response.sendError(HttpServletResponse.SC_UNAUTHORIZED);        	
        }		
	}

}
