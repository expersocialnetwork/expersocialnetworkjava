package rest;

import java.io.UnsupportedEncodingException;
import java.security.Key;
import java.util.Date;
import java.util.List;

import javax.crypto.spec.SecretKeySpec;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.UriBuilder;
import javax.xml.bind.DatatypeConverter;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import model.User;



@Stateless
@Path("/login")
public class LoginEndpoint {
	@PersistenceContext(unitName = "primary")
	private EntityManager em;
	
	
	
	@POST
	@Consumes("application/json")
	@Produces("application/json")
	public Response Login(User user) throws UnsupportedEncodingException {	
		User userFound = em.createQuery(
				  "SELECT u from User u WHERE u.username = :username", User.class).
				  setParameter("username", user.getUsername()).getSingleResult();		
		if (userFound.getPassword().equals(user.getPassword())) {
			 			byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary("javaServerSecret");
	   SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;
						Key signingKey = new SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.getJcaName());
						String jwt = Jwts.builder()
								  .setSubject("JavaServer")
								  .claim("id", userFound.getId())
								  .claim("lastname", userFound.getLastname())
								  .claim("firstname", userFound.getFirstname())
								  .claim("username", userFound.getUsername())
								  .claim("email", userFound.getEmail())
								  .signWith(
										  signatureAlgorithm, signingKey
								  )
								  .compact();
						return Response.ok(jwt).build();
		}	
		return Response.status(401).build();
	}
	

}


