package model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(User.class)
public abstract class User_ {

	public static volatile SetAttribute<User, FriendRequest> friendRequests;
	public static volatile SingularAttribute<User, String> firstname;
	public static volatile SingularAttribute<User, String> password;
	public static volatile SetAttribute<User, Publication> likedPublications;
	public static volatile SingularAttribute<User, Long> id;
	public static volatile SetAttribute<User, Publication> createdPublications;
	public static volatile SingularAttribute<User, String> email;
	public static volatile SetAttribute<User, User> friends;
	public static volatile SingularAttribute<User, String> lastname;
	public static volatile SingularAttribute<User, String> username;

}

