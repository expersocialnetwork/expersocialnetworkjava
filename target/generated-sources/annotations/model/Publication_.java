package model;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Publication.class)
public abstract class Publication_ {

	public static volatile SingularAttribute<Publication, Date> createdAt;
	public static volatile SingularAttribute<Publication, User> createdBy;
	public static volatile SetAttribute<Publication, User> likedBy;
	public static volatile SingularAttribute<Publication, Long> id;
	public static volatile SingularAttribute<Publication, String> content;
	public static volatile SingularAttribute<Publication, Date> updatedAt;

}

