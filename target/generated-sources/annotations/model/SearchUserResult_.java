package model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(SearchUserResult.class)
public abstract class SearchUserResult_ {

	public static volatile SingularAttribute<SearchUserResult, String> firstname;
	public static volatile SingularAttribute<SearchUserResult, Long> id;
	public static volatile SingularAttribute<SearchUserResult, String> email;
	public static volatile SingularAttribute<SearchUserResult, String> lastname;
	public static volatile SingularAttribute<SearchUserResult, String> username;

}

