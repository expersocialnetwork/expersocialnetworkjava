package model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(FriendRequest.class)
public abstract class FriendRequest_ {

	public static volatile SingularAttribute<FriendRequest, User> requestingUser;
	public static volatile SingularAttribute<FriendRequest, User> requestedUser;
	public static volatile SingularAttribute<FriendRequest, Long> id;

}

